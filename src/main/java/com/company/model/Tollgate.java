package com.company.model;

/**
 * Placeholder for PSS Gated Project
 */
public class Tollgate extends Work {

    private String activePhase;
    private String approvalMode;

    public String getActivePhase() {
        return activePhase;
    }

    public void setActivePhase(String activePhase) {
        this.activePhase = activePhase;
    }

    public String getApprovalMode() {
        return approvalMode;
    }

    public void setApprovalMode(String approvalMode) {
        this.approvalMode = approvalMode;
    }
}
