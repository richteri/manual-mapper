package com.company.mapper;

import com.company.dto.TollgateDTO;
import com.company.model.Tollgate;
import com.company.pss.InstallationContext;

/**
 * Tollgate mapper that reuses WorkMapper to emulate inheritance
 */
public class TollgateMapper /* extends WorkMapper */ implements Mapper<Tollgate, TollgateDTO> {
    public static final FieldMap DEFAULT_FIELDS =
            // extend default work fields
            new FieldMap(WorkMapper.DEFAULT_FIELDS, "activePhase,approvalMode");

    @Override
    public FieldMap getDefaultFields() {
        return DEFAULT_FIELDS;
    }

    @Override
    public void toDTO(Tollgate obj, TollgateDTO dto, FieldMap fields) {
        // conflict: super.toDTO(obj, dto, fields);
        new WorkMapper().toDTO(obj, dto, fields);

        if (fields.has("activePhase"))
            dto.setActivePhase(obj.getActivePhase());

        if (fields.has("approvalMode"))
            dto.setApprovalMode(obj.getApprovalMode());
    }

    @Override
    public Tollgate toObject(TollgateDTO dto, InstallationContext context) {

        // conflict: Tollgate obj = (Tollgate) super.toObject(dto, context);
        Tollgate obj = (Tollgate) new WorkMapper().toObject(dto, context);

        if (obj != null) {

            if (dto.getActivePhase() != null)
                obj.setActivePhase(dto.getActivePhase());

            if (dto.getApprovalMode() != null)
                obj.setApprovalMode(dto.getApprovalMode());

        }

        return obj;
    }

}
