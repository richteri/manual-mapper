package com.company.mapper;

import com.company.dto.UserDTO;
import com.company.dto.WorkDTO;
import com.company.model.Work;
import com.company.pss.InstallationContext;
import com.company.pss.PSObject;

/**
 * A mapper that reuses UserMapper for the embedded object
 */
public class WorkMapper implements Mapper<Work, WorkDTO> {

    // user means default fields of the user DTO
    public static final FieldMap DEFAULT_FIELDS =
            new FieldMap("id,name,status,priority,description,start,end,rateTable,budget,user");

    @Override
    public FieldMap getDefaultFields() {
        return DEFAULT_FIELDS;
    }

    @Override
    public void toDTO(Work obj, WorkDTO dto, FieldMap fields) {
        dto.setId(obj.getId());

        if (fields.has("name"))
            dto.setName(obj.getName());

        if (fields.has("status"))
            dto.setStatus(obj.getStatus());

        if (fields.has("priority"))
            dto.setPriority(obj.getPriority());

        if (fields.has("description"))
            dto.setDescription(obj.getDescription());

        if (fields.has("start"))
            dto.setStart(obj.getStart());

        if (fields.has("end"))
            dto.setEnd(obj.getEnd());

        if (fields.has("rateTable"))
            dto.setRateTable(obj.getRateTable());

        if (fields.has("budget"))
            dto.setBudget(obj.getBudget());

        if (fields.has("user")) {
            UserDTO userDTO = new UserDTO();
            new UserMapper().toDTO(obj.getUser(), userDTO, fields.get("user"));
            dto.setUser(userDTO);
        }
    }

    @Override
    public Work toObject(WorkDTO dto, InstallationContext context) {
        Work obj = (Work) PSObject.get(dto.getId(), context);

        if (obj != null) {

            if (dto.getName() != null)
                obj.setName(dto.getName());

            if (dto.getStatus() != null)
                obj.setStatus(dto.getStatus());

            if (dto.getPriority() != null)
                obj.setPriority(dto.getPriority());

            if (dto.getDescription() != null)
                obj.setDescription(dto.getDescription());

            if (dto.getStart() != null)
                obj.setStart(dto.getStart());

            if (dto.getEnd() != null)
                obj.setEnd(dto.getEnd());

            if (dto.getRateTable() != null)
                obj.setRateTable(dto.getRateTable());

            if (dto.getBudget() != null)
                obj.setBudget(dto.getBudget());
        }

        return obj;
    }

}
