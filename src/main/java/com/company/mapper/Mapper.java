package com.company.mapper;

import com.company.dto.BaseDTO;
import com.company.pss.InstallationContext;

/**
 * The base mapper interface that enforces mapper structure
 */
public interface Mapper<S, D extends BaseDTO> {
    FieldMap getDefaultFields();

    void toDTO(S obj, D dto, FieldMap fields);

    default void toDTO(S obj, D dto) {
        toDTO(obj, dto, getDefaultFields());
    }

    S toObject(D dto, InstallationContext context);
}
