package com.company.mapper;

import com.company.dto.UserDTO;
import com.company.model.User;
import com.company.pss.InstallationContext;
import com.company.pss.PSObject;

/**
 * A basic mapper
 */
public class UserMapper implements Mapper<User, UserDTO> {

    public static final FieldMap DEFAULT_FIELDS = new FieldMap("id,name,email");

    @Override
    public FieldMap getDefaultFields() {
        return DEFAULT_FIELDS;
    }

    @Override
    public void toDTO(User obj, UserDTO dto, FieldMap fields) {
        dto.setId(obj.getId());

        if (fields.has("name"))
            dto.setName(obj.getName());

        if (fields.has("email"))
            dto.setEmail(obj.getEmail());
    }

    @Override
    public User toObject(UserDTO dto, InstallationContext context) {
        User obj = (User) PSObject.get(dto.getId(), context);
        if (obj != null) {
            if (dto.getName() != null)
                obj.setName(dto.getName());

            if (dto.getEmail() != null)
                obj.setEmail(dto.getEmail());
        }

        return obj;
    }

}
