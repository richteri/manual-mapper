package com.company.mapper;

import com.company.dto.BaseDTO;
import com.company.dto.Registry;

import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

/**
 * FieldMap is a read-only tree that describes object structure on multiple levels
 */
public class FieldMap {

    private final Map<String, FieldMap> fieldMap;

    /**
     * Build map from a query parameter, eg. fields=id,name,user.id,user.name
     *
     * @param queryParameter the parameter value to parse
     */
    public FieldMap(String queryParameter) {
        fieldMap = parseQueryParameter(queryParameter);
    }

    /**
     * This constructor would facilitate inheritance.
     * eg. Tollgate can inherit and override (but not hide) work fields
     *
     * @param fieldMap thew base field map to be extended
     * @param queryParameter the list that holds the extended fields
     */
    public FieldMap(FieldMap fieldMap, String queryParameter) {
        this.fieldMap = new LinkedHashMap<>(fieldMap.getFieldMap());
        this.fieldMap.putAll(parseQueryParameter(queryParameter));
    }

    private Map<String, FieldMap> parseQueryParameter(String queryParameter) {
        Map<String, FieldMap> map = new LinkedHashMap<>();
        // TODO implement and decide if we needed validation
        return map;
    }

    private Map<String, FieldMap> getFieldMap() {
        return fieldMap;
    }

    public FieldMap get(String fieldName) {
        return fieldMap.get(fieldName);
    }

    public boolean has(String fieldName) {
        return fieldMap.containsKey(fieldName);
    }

    /**
     * This method would check if all queried fields are present in the DTO
     * @param dto the DTO to validate field list against
     */
    public void validate(BaseDTO dto) {
        FieldMap allFields = Registry.DTO_FIELD_MAP.get(dto.getClass());
        Set<String> notFound = new HashSet<>(fieldMap.keySet());
        notFound.removeAll(allFields.getFieldMap().keySet());
        if (!notFound.isEmpty()) {
            throw new IllegalArgumentException("Field(s) not found: " + notFound);
        }
    }
}
