package com.company.dto;

/**
 * Tollgate DTO shows how inheritance would work
 */
public class TollgateDTO extends WorkDTO {

    private String activePhase;
    private String approvalMode;

    public String getActivePhase() {
        return activePhase;
    }

    public void setActivePhase(String activePhase) {
        this.activePhase = activePhase;
    }

    public String getApprovalMode() {
        return approvalMode;
    }

    public void setApprovalMode(String approvalMode) {
        this.approvalMode = approvalMode;
    }

}
