package com.company.dto;

import com.company.mapper.FieldMap;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.type.filter.AssignableTypeFilter;

import java.beans.Introspector;
import java.lang.reflect.Method;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.StringJoiner;

/**
 * A DTO - FieldMap registry could be used to list all available DTO fields and validate queries.
 * This example uses Spring to scan all BaseDTO subclasses.
 */
public class Registry {

    public static final Map<Class<? extends BaseDTO>, FieldMap> DTO_FIELD_MAP;

    static {
        final Map<Class<? extends BaseDTO>, FieldMap> map = new HashMap<>();

        // Create scanner and disable default filters (that is the 'false' argument)
        final ClassPathScanningCandidateComponentProvider provider =
                new ClassPathScanningCandidateComponentProvider(false);

        // Add include filter which matches all classes that extends BaseDTO
        provider.addIncludeFilter(new AssignableTypeFilter(BaseDTO.class));

        // Get matching classes defined in the package
        final Set<BeanDefinition> classes = provider.findCandidateComponents("com.company.dto");

        // Build field name map from getters
        for (BeanDefinition bean : classes) {
            try {
                Class<? extends BaseDTO> clazz = (Class<? extends BaseDTO>) Class.forName(bean.getBeanClassName());
                StringJoiner fields = new StringJoiner(",");
                for (Method method : clazz.getMethods()) {
                    if (method.getName().startsWith("is")) {
                        fields.add(Introspector.decapitalize(method.getName().substring(2)));
                    } else if (method.getName().startsWith("has") || method.getName().startsWith("get")) {
                        fields.add(Introspector.decapitalize(method.getName().substring(2)));
                    }
                    map.put(clazz, new FieldMap(fields.toString()));
                }
            } catch (ClassNotFoundException e) {
                // TODO throw something meaningful
                throw new RuntimeException(e);
            }
        }

        DTO_FIELD_MAP = Collections.unmodifiableMap(map);
    }

}
